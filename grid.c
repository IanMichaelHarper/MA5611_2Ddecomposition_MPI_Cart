#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <mpi.h>

#define ROWS 20
#define COLS 10
	
int rank, size, lrows, lcols;

void print_grid(double **g) {

	int i, j, k;

	for(k=0;k<size;k++) {
		if(rank == k) {
			printf("------------------rank %d grid -------------------------------------\n\n", rank);
			for(i=1;i<lrows-1;i++) {
				for(j=1;j<lcols-1;j++)
					printf("%2.2f\t", g[i][j]);
				printf("\n");
			}
		}
		fflush(stdout);
		usleep(500);	
		MPI_Barrier(MPI_COMM_WORLD);
	}
	if(rank == 0) {
		printf("-----------------------------------\n");
		fflush(stdout);
	}
	usleep(500);	
	MPI_Barrier(MPI_COMM_WORLD);
}

int main(int argc, char *argv[]) {

	double **g1, **g2, **g3;
	double *tmp;	
	int i, j, k;
	int srow, erow, down, up;
	MPI_Status stats[8];	
	MPI_Request reqs[8];

	MPI_Init(&argc, &argv);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	MPI_Comm_size(MPI_COMM_WORLD, &size);

	//create dims
	int ndims = 2;
	int dims[ndims];
	dims[0] = 0; dims[1] = 0;
	MPI_Dims_create(size, ndims, dims);
 
	//create comm_cart
	int periods[ndims];
	periods[0] = 0; periods[1] = 0; // 1 = True
	int reorder = 0;  // 0 = False
	MPI_Comm comm_cart;
	MPI_Cart_create(MPI_COMM_WORLD, ndims, dims, periods, reorder, &comm_cart);

	lrows = ROWS/dims[0] + 2;
	lcols = COLS/dims[1] + 2;

	// Allocate grids
	g1 = malloc(lrows * sizeof(double *));
	tmp = malloc(lrows*lcols*sizeof(double));
	for(i=0;i<lrows;i++) {
		g1[i] = &tmp[lcols*i];
	}
	
	g2 = malloc(lrows * sizeof(double *));
	tmp = malloc(lrows*lcols*sizeof(double));
	for(i=0;i<lrows;i++) {
		g2[i] = &tmp[lcols*i];
	}
	

	// Initial boundary conditions
	// Top is 100
	if(rank < dims[0])   //all ranks that hold top of grid
		for(i=0;i<lcols;i++)
			g1[1][i] = g2[1][i] = 100;

	// Bottom is 0
	if(rank >= size - dims[0])
		for(i=0;i<lcols;i++)
			g1[lrows-2][i] = g2[lrows-2][i] = 0;

	// Left is 40
	if (rank%dims[0] == 0)
		for(i=0;i<lrows;i++)
			g1[i][1] = g2[i][1] = 40;   
	// Right is 60
	if (rank%dims[0] == dims[0]-1)
		for(i=0;i<lrows;i++)
			g1[i][lcols-2] = g2[i][lcols-2] = 60; 	
	
	print_grid(g1);

	// if(rank == 0) {
		// MPI_Abort(MPI_COMM_WORLD, 77);
	// }

	// Solve using jacobi (k iterations)
	
	srow = 1; erow = lrows-1;
	int scol = 1; int ecol = lcols-1;
	
	//create coltype
	MPI_Datatype coltype;
	MPI_Type_vector(lrows-2, 1, lcols, MPI_DOUBLE, &coltype);
	MPI_Type_commit( &coltype );

	//get coords
	int coords[ndims];
	MPI_Cart_coords(comm_cart, rank, ndims, coords);
	int u, d, l ,r;   //up down left right ranks
	
	//shift
	MPI_Cart_shift(comm_cart, 1, 1, &l, &r);
	MPI_Cart_shift(comm_cart, 0, 1, &d, &u);

	//change srow scol etc. to avoid updating boundary 
	if(coords[0] == 0) {
		srow = 2;
	}
	if(coords[0] == dims[1]-1) {
		erow = lrows-2;
	}
	if (coords[1] == 0) {
		scol = 2;
	}
	if (coords[1] == dims[0]-1)
	{
		ecol = lcols-2;
	}
	
	//iteration
	for(k=0;k<100;k++) {
		// Irecv into g2 halo rows
		//printf("%d: Irecv into rows %d %d\n", rank, 0, lrows-1);
		MPI_Irecv(&g2[0][1], lcols, MPI_DOUBLE, u, 0, comm_cart, &reqs[0]);
		MPI_Irecv(&g2[lrows-1][1], lcols, MPI_DOUBLE, d, 0, comm_cart, &reqs[1]);
		// Irecv into g2 halo rows
		//printf("%d: Irecv into rows %d %d\n", rank, 0, lrows-1);
		//MPI_Cart_rank(comm_cart, coords, &src);
		MPI_Irecv(&g2[1][0], 1, coltype, l, 0, comm_cart, &reqs[2]);	
		MPI_Irecv(&g2[1][lcols-1], 1, coltype, r, 0, comm_cart, &reqs[3]);
		
		// Fill rows we want to send to other people
		i=srow;
		//printf("rank %d: srow = %d\n", rank, srow);
		//printf("%d: Updating row %d\n", rank, i);
		for(j=scol;j<ecol-1;j++)
			g2[i][j] = 0.25 * (g1[i-1][j] + g1[i+1][j] + g1[i][j-1] +  g1[i][j+1]);

		i=erow-1;
		//printf("rank %d: erow-1 = %d\n", rank, erow-1);
		//printf("%d: Updating row %d\n", rank, i);
		for(j=scol;j<ecol-1;j++)
			g2[i][j] = 0.25 * (g1[i-1][j] + g1[i+1][j] + g1[i][j-1] +  g1[i][j+1]);
	
		// Fill cols we want to send to other people
		j=scol;
		//printf("rank %d: scol = %d\n", rank, scol);
		//printf("%d: Updating col %d\n", rank, j);
		for(i=srow;i<erow-1;i++)
			g2[i][j] = 0.25 * (g1[i-1][j] + g1[i+1][j] + g1[i][j-1] +  g1[i][j+1]);

		j=ecol-1;
		//printf("rank %d: ecol-1 = %d\n", rank, ecol-1);
		//printf("%d: Updating col %d\n", rank, j);
		for(i=srow;i<erow;i++)
			g2[i][j] = 0.25 * (g1[i-1][j] + g1[i+1][j] + g1[i][j-1] +  g1[i][j+1]);
		
		
		// Isend our halo rows
		// printf("%d: Isend from rows %d %d\n", rank, 1, lrows-2);
		MPI_Isend(&g2[lrows-2][1], lcols, MPI_DOUBLE, d, 0, comm_cart, &reqs[4]);  
		MPI_Isend(&g2[1][1], lcols, MPI_DOUBLE, u, 0, comm_cart, &reqs[5]);
		// Isend our halo cols
		// printf("%d: Isend from cols %d %d\n", rank, 1, lcols-2);
		MPI_Isend(&g2[1][lcols-2], 1, coltype, r, 0, comm_cart, &reqs[6]);
		MPI_Isend(&g2[1][1], 1, coltype, l, 0, comm_cart, &reqs[7]);
		
		// Fill internal points
		// printf("%d: Updating from %d to %d\n", rank, srow+1, erow-2);
		for(i=srow+1;i<erow-1;i++) {
			for(j=scol+1;j<ecol-1;j++) {
				g2[i][j] = 0.25 * (g1[i-1][j] + g1[i+1][j] + g1[i][j-1] +  g1[i][j+1]);
			}
		}
		
		// Wait on Isend and Irecv
		//for(i=0;i<4;i++) {
			//MPI_Wait(&reqs[i], &stats[i]);
		//}
		MPI_Waitall(8, reqs, stats);
	
		// Swap
		g3 = g2;
		g2 = g1;
		g1 = g3;
	}

	print_grid(g1);
		
	MPI_Finalize();
	exit(0);
}

