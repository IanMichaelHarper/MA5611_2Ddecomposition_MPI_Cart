prog: grid.c
	mpicc grid.c -o prog
test: grid.c
	mpicc grid.c -o prog
	mpirun -np 4 ./prog
clean:
	rm prog
